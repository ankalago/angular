import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { ProductFormComponent } from './components/product-form/product-form.component';
import {MaterialModule} from '../material/material.module';
import { NavigationComponent } from './components/navigation/navigation.component';


@NgModule({
  declarations: [ProductFormComponent, NavigationComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
  ]
})
export class AdminModule { }
