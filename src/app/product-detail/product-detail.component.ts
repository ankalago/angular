import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {ProductsService} from '../core/services/products/products.service';
import {Product} from '../../types/types';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: Product;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductsService
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.fetchProduct(id);
    });
  }

  fetchProduct = (id) => {
    this.productService.getById(id).subscribe(product => {
      console.log('************* productGet: ', product);
      this.product = product;
    });
    console.log('************* this.product: ', this.product);
  }

  createProduct = () => {
    const id = Math.floor(Math.random() * 100);
    const newProduct: Product = {
      id: String(id),
      image: 'https://picsum.photos/200/300',
      title: `title - ${id}`,
      price: 12000,
      description: `description - ${id}`
    };
    this.productService.createProduct(newProduct).subscribe(product => console.log(product));
  }

  updateProduct = (id) => {
    const updateProduct: Partial<Product> = {
      price: 12000
    };
    this.productService.updateProduct(id, updateProduct).subscribe(product => console.log(product));
  }

  deleteProduct = (id) => {
    this.productService.deleteProduct(id).subscribe(product => console.log(product));
  }
}
