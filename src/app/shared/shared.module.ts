import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HighlightDirective} from './directives/highlight/highlight.directive';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {ExponentialPipe} from './pipes/exponential/exponential.pipe';
import {RouterModule} from '@angular/router';
import {MaterialModule} from '../material/material.module';



@NgModule({
  declarations: [
    HighlightDirective,
    HeaderComponent,
    FooterComponent,
    ExponentialPipe
  ],
  exports: [
    HighlightDirective,
    ExponentialPipe,
    HeaderComponent,
    FooterComponent,
    MaterialModule
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule
  ]
})
export class SharedModule { }
