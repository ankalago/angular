import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
  title = 'platzi-store';
  test = 'pool';
  power = 10;
  data: string[] = ['pepe', 'pepa', 'papa'];
  addItem = (value: string) => {
    this.data.push(value);
    this.title = '';
  }
  removeItem = (index: number) => this.data.splice(index, 1);
  constructor() { }

  ngOnInit(): void {
  }

}
