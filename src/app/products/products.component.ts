import { Component, OnInit } from '@angular/core';
import {Product} from '../../types/types';
import {ProductsService} from '../core/services/products/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: Product[];
  constructor(
    private productService: ProductsService
  ) { }

  ngOnInit(): void {
    this.fetchProducts();
  }

  fetchProducts = () => {
    this.productService.getAllProducts().subscribe((products) => {
      this.products = products;
    });
  }

  addItemCartSuccess = (text) => console.log(text);
  removeItemCartSuccess = (id) => this.products.splice(id, 1);

}
