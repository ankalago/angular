import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  emailField: FormControl;
  value = 'Clear me';

  constructor(
  ) {
    this.emailField = new FormControl('test@test.com', [
      Validators.minLength(4),
      Validators.maxLength(10)
    ]);
    // this.emailField.valueChanges.subscribe(value => console.log('************* value: ', value));
  }

  ngOnInit(): void {
  }

  sendForm = () => {
    if (this.emailField.valid) {
      console.log('************* send: ');
    }
  }

}
