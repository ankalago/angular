import { NgModule } from '@angular/core';

import {BannerComponent} from './components/banner/banner.component';
import {HomeComponent} from './components/home/home.component';
import {HomeRoutingModule} from './home-routing.module';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {CoreModule} from '../core/core.module';
import {MaterialModule} from '../material/material.module';

@NgModule({
  declarations: [
    HomeComponent,
    BannerComponent,
  ],
  imports: [
    HomeRoutingModule,
    SharedModule,
    CoreModule,
    CommonModule,
    MaterialModule
  ],
})
export class HomeModule { }
