// decoradores
import {Component, Input, Output, EventEmitter, OnChanges, OnInit, SimpleChanges, DoCheck, OnDestroy} from '@angular/core';
import {Product} from '../../types/types';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnChanges, OnInit, DoCheck, OnDestroy {
  @Input() product: Product;
  @Output() productClicked: EventEmitter<string> = new EventEmitter<string>();
  @Output() removeClicked: EventEmitter<string> = new EventEmitter<string>();
  today: Date = new Date();

  constructor() {
    console.log('************* 1. constructor: ');
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('************* 2. ngOnChange: ');
    console.log('************* changes: ', changes);
  }

  ngOnInit() {
    console.log('************* 3. ngOnInit: ');
    console.log('************* product: ', this.product);
    // add call API
  }

  ngDoCheck() {
    // si se quiere usar est emetodo se debe eliminar o comentar ngOnChanges (segun Platzi)
    console.log('************* 4. ngDoCheck: ');
  }

  ngOnDestroy() {
    console.log('************* ngOnDestroy: ');
  }

  addItemCart = () => {
    this.productClicked.emit(`add success cart - ${this.product.title}`);
  }

  removeItemCart = () => {
    this.removeClicked.emit(this.product.id);
  }
}
