import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Product} from '../../../../types/types';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products: Product[];

  constructor(
    private http: HttpClient
  ) { }

  getAllProducts = () => this.http.get<Product[]>(`${environment.urlAPI}/products`);

  getById = (id) => this.http.get<Product>(`${environment.urlAPI}/products/${id}`);

  createProduct = (product: Product) => this.http.post<Product>(`${environment.urlAPI}/products`, product);

  updateProduct = (id, changes: Partial<Product>) => this.http.put<Product>(`${environment.urlAPI}/products/${id}`, changes);

  deleteProduct = (id) => this.http.delete<Product>(`${environment.urlAPI}/products/${id}`);
}
