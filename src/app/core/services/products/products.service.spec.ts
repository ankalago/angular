import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ProductsService } from './products.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

describe('ProductsService', () => {
  let service: ProductsService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  const expectData = [
    {
      id: '1',
      title: 'title',
      price: 1,
      description: 'description',
      image: 'image'
    },
    {
      id: '1',
      title: 'title',
      price: 1,
      description: 'description',
      image: 'image'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ProductsService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('test for getAllProducts', () => {
    it('should return products', () => {
      let dataError;
      let dataResponse;

      service.getAllProducts()
        .subscribe(x => {
          dataResponse = x;
        }, error => {
          dataError = error;
        });

      const req = httpTestingController.expectOne(`${environment.urlAPI}/products`);
      req.flush(expectData);

      expect(dataResponse.length).toEqual(2);
    });
  });

});
